#include "qemu/osdep.h"
#include "qemu/units.h"
#include "hw/pci/pci.h"
#include "hw/pci/msix.h"
#include "qemu/timer.h"
#include "qemu/main-loop.h" /* iothread mutex */
#include "qapi/visitor.h"
#include "qemu/thread.h"

#include "hw/misc/hl_goya/goya/goya.h"
#include "hw/misc/hl_goya/goya/goya_packets.h"
#include "hw/misc/hl_goya/hl_boot_if.h"
#include "hw/misc/hl_goya/hw_ip/mmu/mmu_v1_0.h"
#include "hw/misc/hl_goya/armcp_if.h"
#include "hw/misc/hl_goya/qman_if.h"
#include "hw/misc/hl_goya/goya/asic_reg/goya_masks.h"

#define TYPE_PCI_HL_GOYA_DEVICE "hl_goya"
#define HL_GOYA(obj)        OBJECT_CHECK(HLGoyaState, obj, TYPE_PCI_HL_GOYA_DEVICE)

#define PCI_VENDOR_ID_HABANALABS	0x1da3
#define PCI_IDS_GOYA			0x0001

/* Registers */
#define HL_GOYA_NUM_IRQS		8

/* MSI-X memory definitions */
#define HL_GOYA_MSIX_TABLE_OFFSET	0x0000
#define HL_GOYA_MSIX_PBA_OFFSET		0x500

#define FW_UBOOT_VERSION_ADDR		0x00a5
#define FW_UBOOT_VERSION		0x0001

#define FW_PREBOOT_VERSION_ADDR		0x01a5
#define FW_PREBOOT_VERSION		0x0001

#define GOYA_QMAN0_FENCE_VAL		0xD169B243

/* TPC definitions */
#define TPC_OFFSET(i) ((i) * 0x40000)
#define TPC_CFG_FUNC_MBIST_CNTRL(i) \
	(mmTPC0_CFG_FUNC_MBIST_CNTRL + TPC_OFFSET((i)))
#define TPC_EML_OFFSET(i) ((i) * 0x200000)
#define TPC_EML_ADDRESS(i) \
	(mmTPC0_EML_CFG_BASE + TPC_EML_OFFSET((i)) - CFG_BASE)

#define TPC_SLM_OFFSET(i) (TPC_EML_ADDRESS((i) + 0x100000))

#define NUMBER_OF_EXT_HW_QUEUES		5

#define DMA_ADDR_OFFSET(i) ((i) * (mmDMA_QM_1_PQ_PI - mmDMA_QM_0_PQ_PI))

#define mmDMA_QM_0_PQ_ADDR_LO mmDMA_QM_0_PQ_BASE_LO + DMA_ADDR_OFFSET(0)
#define mmDMA_QM_1_PQ_ADDR_LO mmDMA_QM_0_PQ_BASE_LO + DMA_ADDR_OFFSET(1)
#define mmDMA_QM_2_PQ_ADDR_LO mmDMA_QM_0_PQ_BASE_LO + DMA_ADDR_OFFSET(2)
#define mmDMA_QM_3_PQ_ADDR_LO mmDMA_QM_0_PQ_BASE_LO + DMA_ADDR_OFFSET(3)
#define mmDMA_QM_4_PQ_ADDR_LO mmDMA_QM_0_PQ_BASE_LO + DMA_ADDR_OFFSET(4)

#define mmDMA_QM_0_PQ_ADDR_HI mmDMA_QM_0_PQ_BASE_HI + DMA_ADDR_OFFSET(0)
#define mmDMA_QM_1_PQ_ADDR_HI mmDMA_QM_0_PQ_BASE_HI + DMA_ADDR_OFFSET(1)
#define mmDMA_QM_2_PQ_ADDR_HI mmDMA_QM_0_PQ_BASE_HI + DMA_ADDR_OFFSET(2)
#define mmDMA_QM_3_PQ_ADDR_HI mmDMA_QM_0_PQ_BASE_HI + DMA_ADDR_OFFSET(3)
#define mmDMA_QM_4_PQ_ADDR_HI mmDMA_QM_0_PQ_BASE_HI + DMA_ADDR_OFFSET(4)

#define HL_QUEUE_LENGTH 256

#define HL_CPU_PKT_SHIFT		5
#define HL_CPU_PKT_SIZE			(1 << HL_CPU_PKT_SHIFT)
#define HL_CPU_PKT_MASK			(~((1 << HL_CPU_PKT_SHIFT) - 1))

#define DMA_QM_OFFSET (mmDMA_QM_1_GLBL_STS0 - mmDMA_QM_0_GLBL_STS0)
#define DMA_QM_REG(i) (mmDMA_QM_0_GLBL_STS0 + (i) * DMA_QM_OFFSET)

#define TPC_QM_OFFSET (mmTPC1_QM_GLBL_STS0 - mmTPC0_QM_GLBL_STS0)
#define TPC_QM_REG(i) (mmTPC0_QM_GLBL_STS0 + (i) * TPC_QM_OFFSET)
#define TPC_CMDQ_REG(i) (mmTPC0_CMDQ_GLBL_STS0 + (i) * TPC_QM_OFFSET)
#define TPC_CFG_REG(i) (mmTPC0_CFG_STATUS + (i) * TPC_QM_OFFSET)

enum goya_queue_id {
	GOYA_QUEUE_ID_DMA_0 = 0,
	GOYA_QUEUE_ID_DMA_1,
	GOYA_QUEUE_ID_DMA_2,
	GOYA_QUEUE_ID_DMA_3,
	GOYA_QUEUE_ID_DMA_4,
	GOYA_QUEUE_ID_CPU_PQ,
	GOYA_QUEUE_ID_MME,
	GOYA_QUEUE_ID_TPC0,
	GOYA_QUEUE_ID_TPC1,
	GOYA_QUEUE_ID_TPC2,
	GOYA_QUEUE_ID_TPC3,
	GOYA_QUEUE_ID_TPC4,
	GOYA_QUEUE_ID_TPC5,
	GOYA_QUEUE_ID_TPC6,
	GOYA_QUEUE_ID_TPC7,
	GOYA_QUEUE_ID_SIZE
};

enum hl_queue_type {
	QUEUE_TYPE_NA,
	QUEUE_TYPE_EXT,
	QUEUE_TYPE_INT,
	QUEUE_TYPE_CPU
};

typedef struct {
	PCIDevice pdev;
	MemoryRegion sram_cfg;
	MemoryRegion msix;
	MemoryRegion ddr;
	QemuMutex lock;

	char dma_buf[sizeof(struct hl_bd) * NUMBER_OF_EXT_HW_QUEUES];
	dma_addr_t hw_queue_phys_addr[GOYA_QUEUE_ID_SIZE];
	char ext_queue[NUMBER_OF_EXT_HW_QUEUES]
		[HL_QUEUE_LENGTH * sizeof(struct hl_bd)];
	uint32_t ext_queue_offset[NUMBER_OF_EXT_HW_QUEUES];
	char cpu_queue[HL_QUEUE_LENGTH * sizeof(struct hl_bd)];
	uint32_t cpu_queue_offset;
	enum cpu_boot_status boot_status;

	uint32_t version;
	bool dirty;

	unsigned long irq_status;
} HLGoyaState;

#if 0
static void print_log(const char *fmt, ...)
{
	FILE *fp = fopen("qemu.log", "a");

	va_list args;
	va_start(args, fmt);
	vfprintf(fp, fmt, args);
	va_end(args);
	fclose(fp);
}
#endif

static void hl_goya_raise_irq(HLGoyaState *hl_goya, uint8_t irq_num)
{
	if (irq_num > 31) {
		return;
	}

	if (msix_enabled(&hl_goya->pdev)) {
		msix_notify(&hl_goya->pdev, irq_num);
	} else {
		hw_error("MSI-X disabled\n");
	}

	set_bit(irq_num, &hl_goya->irq_status);
}

static uint64_t hl_goya_sram_cfg_read(void *opaque, hwaddr addr, unsigned size)
{
	HLGoyaState *hl_goya = opaque;
	uint64_t val = ~0ULL;
	uint32_t offset = addr - (CFG_BASE - SRAM_BASE_ADDR);

	switch (offset) {
		case mmPCIE_DBI_DEVICE_ID_VENDOR_ID_REG:
			val = hl_goya->version;
			break;
		case mmPSOC_GLOBAL_CONF_SCRATCHPAD_29:
			val = FW_UBOOT_VERSION_ADDR;
			break;
		case FW_UBOOT_VERSION_ADDR:
			val = FW_UBOOT_VERSION;
			break;
		case mmPSOC_GLOBAL_CONF_SCRATCHPAD_28:
			val = FW_PREBOOT_VERSION_ADDR;
			break;
		case FW_PREBOOT_VERSION_ADDR:
			val = FW_PREBOOT_VERSION;
			break;
		case mmPSOC_GLOBAL_CONF_WARM_REBOOT:
			val = hl_goya->boot_status;
			break;
		case mmPSOC_GLOBAL_CONF_BOOT_STRAP_PINS:
			val = ~PSOC_GLOBAL_CONF_BOOT_STRAP_PINS_SRIOV_EN_MASK;
			break;
		case TPC_CFG_FUNC_MBIST_CNTRL(0):
		case TPC_CFG_FUNC_MBIST_CNTRL(1):
		case TPC_CFG_FUNC_MBIST_CNTRL(2):
		case TPC_CFG_FUNC_MBIST_CNTRL(3):
		case TPC_CFG_FUNC_MBIST_CNTRL(4):
		case TPC_CFG_FUNC_MBIST_CNTRL(5):
		case TPC_CFG_FUNC_MBIST_CNTRL(6):
		case TPC_CFG_FUNC_MBIST_CNTRL(7):
		case TPC_CFG_FUNC_MBIST_CNTRL(8):
		case TPC_CFG_FUNC_MBIST_CNTRL(9):
			val = TPC0_CFG_FUNC_MBIST_CNTRL_MBIST_DONE_MASK;
			break;
		case MMU_ASID_BUSY:
			val = ~0x80000000;
			break;
		case mmSTLB_INV_ALL_START:
			val = 0;
			break;
		case mmSTLB_INV_CONSUMER_INDEX:
			val = 0;
			break;
		case mmPSOC_GLOBAL_CONF_SCRATCHPAD_7:
			val = PQ_INIT_STATUS_READY_FOR_HOST;
			break;
		case DMA_QM_REG(0):
		case DMA_QM_REG(1):
		case DMA_QM_REG(2):
		case DMA_QM_REG(3):
		case DMA_QM_REG(4):
			val = DMA_QM_IDLE_MASK;
			break;
		case TPC_QM_REG(0):
		case TPC_QM_REG(1):
		case TPC_QM_REG(2):
		case TPC_QM_REG(3):
		case TPC_QM_REG(4):
			val = TPC_QM_IDLE_MASK;
			break;
		case TPC_CMDQ_REG(0):
		case TPC_CMDQ_REG(1):
		case TPC_CMDQ_REG(2):
		case TPC_CMDQ_REG(3):
		case TPC_CMDQ_REG(4):
			val = TPC_CMDQ_IDLE_MASK;
			break;
		case TPC_CFG_REG(0):
		case TPC_CFG_REG(1):
		case TPC_CFG_REG(2):
		case TPC_CFG_REG(3):
		case TPC_CFG_REG(4):
			val = TPC_CFG_IDLE_MASK;
			break;
		case mmMME_QM_GLBL_STS0:
			val = MME_QM_IDLE_MASK;
			break;
		case mmMME_CMDQ_GLBL_STS0:
			val = MME_CMDQ_IDLE_MASK;
			break;
		case mmMME_ARCH_STATUS:
			val = MME_ARCH_IDLE_MASK;
			break;
		case mmMME_SHADOW_0_STATUS:
			val = ~MME_SHADOW_IDLE_MASK;
			break;
	}

	return val;
}

static void send_dma_pkt(HLGoyaState *hl_goya, enum goya_queue_id qid,
		uint32_t db_value)
{
	struct packet_msg_prot *pmp_ptr;
	void *pmp_buf;
	dma_addr_t real_q_addr = hl_goya->hw_queue_phys_addr[qid] - HOST_PHYS_BASE;
	struct hl_bd *bd = (struct hl_bd *)&hl_goya->ext_queue[qid];
	pci_dma_read(&hl_goya->pdev, real_q_addr, bd,
			HL_QUEUE_LENGTH * sizeof(struct hl_bd));
	if (bd->ptr - HOST_PHYS_BASE > HOST_PHYS_SIZE) {
		return;
	}
	bd += hl_goya->ext_queue_offset[qid];
	hl_goya->ext_queue_offset[qid] = db_value & (HL_QUEUE_LENGTH - 1);
	pmp_buf = malloc(bd->len);
	pci_dma_read(&hl_goya->pdev, bd->ptr - HOST_PHYS_BASE,
			pmp_buf, bd->len);
	pmp_ptr = (struct packet_msg_prot *)(pmp_buf
		+ bd->len - sizeof(struct packet_msg_prot));
	pmp_ptr->value = cpu_to_le32(GOYA_QMAN0_FENCE_VAL);
	pci_dma_write(&hl_goya->pdev, pmp_ptr->addr - HOST_PHYS_BASE,
			&pmp_ptr->value, sizeof(uint32_t));
	free(pmp_buf);
}

static void send_cpu_pkt(HLGoyaState *hl_goya, uint32_t db_value)
{
	struct armcp_packet pkt;
	struct hl_bd *tmp;

	memset(&pkt, 0, sizeof(struct armcp_packet));

	pci_dma_read(&hl_goya->pdev,
			hl_goya->hw_queue_phys_addr[GOYA_QUEUE_ID_CPU_PQ] -
			HOST_PHYS_BASE, &hl_goya->cpu_queue,
			HL_QUEUE_LENGTH * sizeof(struct hl_bd));
	tmp = (struct hl_bd *)&hl_goya->cpu_queue;
	if (!tmp->ptr) {
		return;
	}

	tmp = (struct hl_bd *)&hl_goya->cpu_queue;
	tmp += hl_goya->cpu_queue_offset;

	hl_goya->cpu_queue_offset = db_value & (HL_QUEUE_LENGTH - 1);
	pci_dma_read(&hl_goya->pdev, tmp->ptr - HOST_PHYS_BASE, &pkt,
			tmp->len);
	pkt.fence = ARMCP_PACKET_FENCE_VAL;
	uint32_t opcode = pkt.ctl >> ARMCP_PKT_CTL_OPCODE_SHIFT;
	dma_addr_t next_addr = tmp->ptr - HOST_PHYS_BASE;

	switch (opcode) {
		case ARMCP_PACKET_TEST:
		case ARMCP_PACKET_FREQUENCY_SET:
		case ARMCP_PACKET_INFO_GET:
		case ARMCP_PACKET_ENABLE_PCI_ACCESS:
		case ARMCP_PACKET_DISABLE_PCI_ACCESS:
			break;
		default:
			return;
	}

	pci_dma_write(&hl_goya->pdev,
				next_addr, &pkt, tmp->len);
	hl_goya_raise_irq(hl_goya, 0);
}

static void hl_goya_sram_cfg_write(void *opaque, hwaddr addr, uint64_t val,
		unsigned size)
{
	HLGoyaState *hl_goya = opaque;
	qemu_mutex_lock(&hl_goya->lock);

	uint32_t offset = addr - (CFG_BASE - SRAM_BASE_ADDR);

	if (size != 4) {
		return;
	}
	switch (offset) {
		case mmPSOC_GLOBAL_CONF_WARM_REBOOT: {
			hl_goya->boot_status = val;
			break;
		}
		case mmPSOC_GLOBAL_CONF_APP_STATUS: {
			hl_goya->dirty = true;
			hl_goya->boot_status = CPU_BOOT_STATUS_SRAM_AVAIL;
			break;
		}
		case mmPSOC_GLOBAL_CONF_BOOT_SEQ_RE_START: {
			hl_goya->dirty = false;
			hl_goya->boot_status = CPU_BOOT_STATUS_DRAM_RDY;
			break;
		}
		case mmDMA_QM_0_PQ_PI: {
			send_dma_pkt(hl_goya, GOYA_QUEUE_ID_DMA_0, val);
			break;
		}
		case mmDMA_QM_1_PQ_PI: {
			send_dma_pkt(hl_goya, GOYA_QUEUE_ID_DMA_1, val);
			break;
		}
		case mmDMA_QM_2_PQ_PI: {
			send_dma_pkt(hl_goya, GOYA_QUEUE_ID_DMA_2, val);
			break;
		}
		case mmDMA_QM_3_PQ_PI: {
			send_dma_pkt(hl_goya, GOYA_QUEUE_ID_DMA_3, val);
			break;
		}
		case mmDMA_QM_4_PQ_PI: {
			send_dma_pkt(hl_goya, GOYA_QUEUE_ID_DMA_4, val);
			break;
		}
		case mmCPU_IF_PF_PQ_PI: {
			if (val == 0) {
				hl_goya->cpu_queue_offset = 0;
			}
			send_cpu_pkt(hl_goya, val);
			break;
		}
		case mmDMA_QM_0_PQ_ADDR_LO:
			hl_goya->hw_queue_phys_addr[GOYA_QUEUE_ID_DMA_0] = val;
			break;
		case mmDMA_QM_0_PQ_ADDR_HI: {
			dma_addr_t shifted_val = ((uint64_t)val) << 32;
			hl_goya->hw_queue_phys_addr[GOYA_QUEUE_ID_DMA_0] |=
				shifted_val;
			break;
		}
		case mmDMA_QM_1_PQ_ADDR_LO:
			hl_goya->hw_queue_phys_addr[GOYA_QUEUE_ID_DMA_1] = val;
			break;
		case mmDMA_QM_1_PQ_ADDR_HI: {
			dma_addr_t shifted_val = ((uint64_t)val) << 32;
			hl_goya->hw_queue_phys_addr[GOYA_QUEUE_ID_DMA_1] |=
				shifted_val;
			break;
		}
		case mmDMA_QM_2_PQ_ADDR_LO:
			hl_goya->hw_queue_phys_addr[GOYA_QUEUE_ID_DMA_2] = val;
			break;
		case mmDMA_QM_2_PQ_ADDR_HI: {
			dma_addr_t shifted_val = ((uint64_t)val) << 32;
			hl_goya->hw_queue_phys_addr[GOYA_QUEUE_ID_DMA_2] |=
				shifted_val;
			break;
		}
		case mmDMA_QM_3_PQ_ADDR_LO:
			hl_goya->hw_queue_phys_addr[GOYA_QUEUE_ID_DMA_3] = val;
			break;
		case mmDMA_QM_3_PQ_ADDR_HI: {
			dma_addr_t shifted_val = ((uint64_t)val) << 32;
			hl_goya->hw_queue_phys_addr[GOYA_QUEUE_ID_DMA_3] |=
				shifted_val;
			break;
		}
		case mmDMA_QM_4_PQ_ADDR_LO:
			hl_goya->hw_queue_phys_addr[GOYA_QUEUE_ID_DMA_4] = val;
			break;
		case mmDMA_QM_4_PQ_ADDR_HI: {
			dma_addr_t shifted_val = ((uint64_t)val) << 32;
			hl_goya->hw_queue_phys_addr[GOYA_QUEUE_ID_DMA_4] |=
				shifted_val;
			break;
		}
		case mmPSOC_GLOBAL_CONF_SCRATCHPAD_0: {
			hl_goya->hw_queue_phys_addr[GOYA_QUEUE_ID_CPU_PQ] =
				val;
			break;
		}
		case mmPSOC_GLOBAL_CONF_SCRATCHPAD_1: {
			dma_addr_t shifted_val = ((uint64_t)val) << 32;
			hl_goya->hw_queue_phys_addr[GOYA_QUEUE_ID_CPU_PQ] |=
				shifted_val;
			break;
		}
		case mmDMA_QM_0_GLBL_CFG0: {
			if (val == 0) {
				hl_goya->ext_queue_offset[GOYA_QUEUE_ID_DMA_0]
					= 0;
			}
			break;
		}
		case mmDMA_QM_1_GLBL_CFG0: {
			if (val == 0) {
				hl_goya->ext_queue_offset[GOYA_QUEUE_ID_DMA_1]
					= 0;
			}
			break;
		}
		case mmDMA_QM_2_GLBL_CFG0: {
			if (val == 0) {
				hl_goya->ext_queue_offset[GOYA_QUEUE_ID_DMA_2]
					= 0;
			}
			break;
		}
		case mmDMA_QM_3_GLBL_CFG0: {
			if (val == 0) {
				hl_goya->ext_queue_offset[GOYA_QUEUE_ID_DMA_3]
					= 0;
			}
			break;
		}
		case mmDMA_QM_4_GLBL_CFG0: {
			if (val == 0) {
				hl_goya->ext_queue_offset[GOYA_QUEUE_ID_DMA_4]
					= 0;
			}
			break;
		}
	}
	qemu_mutex_unlock(&hl_goya->lock);
}

static const MemoryRegionOps hl_goya_sram_cfg_ops = {
	.read = hl_goya_sram_cfg_read,
	.write = hl_goya_sram_cfg_write,
	.endianness = DEVICE_NATIVE_ENDIAN,
};

static uint64_t hl_goya_ddr_read(void *opaque, hwaddr addr, unsigned size)
{
	/* TODO: Add functionality to emulate DDR reads. */
	return 0;
}

static void hl_goya_ddr_write(void *opaque, hwaddr addr, uint64_t val,
		unsigned size)
{
	/* TODO: Add functionality to emulate DDR writes. */
}

static const MemoryRegionOps hl_goya_ddr_ops = {
	.read = hl_goya_ddr_read,
	.write = hl_goya_ddr_write,
	.endianness = DEVICE_NATIVE_ENDIAN,
};

static uint32_t hl_goya_read_config(PCIDevice *pdev, uint32_t addr,
		int len)
{
	/* TODO: Handle config space reads */
	return pci_default_read_config(pdev, addr, len);
}

static void hl_goya_write_config(PCIDevice *pdev, uint32_t addr,
                                uint32_t val, int len)
{
	/* TODO: Handle config space writes */
	pci_default_write_config(pdev, addr, val, len);
}

static void pci_hl_goya_realize(PCIDevice *pdev, Error **errp)
{
	HLGoyaState *hl_goya = HL_GOYA(pdev);
	int i, err;

	pdev->config_write = hl_goya_write_config;
	pdev->config_read = hl_goya_read_config;
	memory_region_init_io(&hl_goya->sram_cfg, OBJECT(hl_goya),
			&hl_goya_sram_cfg_ops, hl_goya, "hl_goya-sram-cfg", CFG_BAR_SIZE);
	pci_register_bar(pdev, SRAM_CFG_BAR_ID, PCI_BASE_ADDRESS_SPACE_MEMORY, &hl_goya->sram_cfg);
	memory_region_init(&hl_goya->msix, OBJECT(hl_goya), "hl_goya-msix", MSIX_BAR_SIZE);
	pci_register_bar(pdev, MSIX_BAR_ID, PCI_BASE_ADDRESS_SPACE_MEMORY, &hl_goya->msix);
	memory_region_init_io(&hl_goya->ddr, OBJECT(hl_goya),
			&hl_goya_ddr_ops, hl_goya, "hl_goya-sram-cfg", DDR_BAR_SIZE);
	pci_register_bar(pdev, DDR_BAR_ID, PCI_BASE_ADDRESS_SPACE_MEMORY, &hl_goya->ddr);

	err = msix_init(pdev, HL_GOYA_NUM_IRQS,
				&hl_goya->msix,
				MSIX_BAR_ID, HL_GOYA_MSIX_TABLE_OFFSET,
				&hl_goya->msix,
				MSIX_BAR_ID, HL_GOYA_MSIX_PBA_OFFSET,
				0, errp);
	if (err) {
		hw_error("Failed to allocate msix vectors\n");
		return;
	}

	for (i = 0; i < HL_GOYA_NUM_IRQS; i++) {
		err = msix_vector_use(pdev, i);
		if (err) {
			hw_error("Failed to use vector %d\n", i);
		}
	}

	hl_goya->version = 0x1ab5;
	hl_goya->dirty = false;
	memset(&hl_goya->ext_queue, 0, sizeof(hl_goya->ext_queue));
	memset(&hl_goya->cpu_queue, 0, sizeof(hl_goya->cpu_queue));
	memset(hl_goya->ext_queue_offset, 0,
			sizeof(hl_goya->ext_queue_offset));
	hl_goya->cpu_queue_offset = 0;
	hl_goya->boot_status = CPU_BOOT_STATUS_NA;

	qemu_mutex_init(&hl_goya->lock);
}

static void pci_hl_goya_uninit(PCIDevice *pdev)
{
	int i;
	HLGoyaState *hl_goya = HL_GOYA(pdev);

	msix_uninit(pdev, &hl_goya->msix, &hl_goya->msix);

	for (i = 0; i < HL_GOYA_NUM_IRQS; i++) {
		msix_vector_unuse(pdev, i);
	}

	qemu_mutex_destroy(&hl_goya->lock);
}

static void hl_goya_instance_init(Object *obj)
{
	HLGoyaState *hl_goya = HL_GOYA(obj);

	hl_goya->irq_status = 0;
}

static void hl_goya_class_init(ObjectClass *class, void *data)
{
	DeviceClass *dc = DEVICE_CLASS(class);
	PCIDeviceClass *k = PCI_DEVICE_CLASS(class);

	k->realize = pci_hl_goya_realize;
	k->exit = pci_hl_goya_uninit;
	k->vendor_id = PCI_VENDOR_ID_HABANALABS;
	k->device_id = PCI_IDS_GOYA;
	k->class_id = PCI_CLASS_OTHERS;
	set_bit(DEVICE_CATEGORY_MISC, dc->categories);
}

static void pci_hl_goya_register_types(void)
{
	static InterfaceInfo interfaces[] = {
		{ INTERFACE_PCIE_DEVICE },
		{ },
	};

	static const TypeInfo hl_goya_info = {
		.name		= TYPE_PCI_HL_GOYA_DEVICE,
		.parent		= TYPE_PCI_DEVICE,
		.instance_size	= sizeof(HLGoyaState),
		.instance_init	= hl_goya_instance_init,
		.class_init	= hl_goya_class_init,
		.interfaces	= interfaces,
	};

	type_register_static(&hl_goya_info);
}
type_init(pci_hl_goya_register_types);
